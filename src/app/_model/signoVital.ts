import { Paciente } from './paciente';
export class SignoVital {
    idSigno: number;
    paciente: Paciente;
    fechaRegistro: string;
    temperatura: string;
    pulso: string;
    ritmoRespiratorio: string;
    
}