import { Component, OnInit } from '@angular/core';
import { Paciente } from '../../../_model/paciente';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { PacienteService } from '../../../_service/paciente.service';
import { SignoService } from '../../../_service/signo.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SignoVital } from '../../../_model/signoVital';
import { map } from 'rxjs/operators';
import { ActivatedRoute, Router, Params } from '@angular/router';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  
  form: FormGroup;

  edicion: boolean;
  idSigno: number;
  pacientes: Paciente[] = [];

  maxFecha: Date = new Date();
  fechaRegistro: Date = new Date();
  temperatura: string;
  pulso: string;
  ritmoRespiratorio: string;

  mensaje: string;
  pacienteSeleccionado: Paciente;
  //utiles para el autocomplete
  myControlPaciente: FormControl = new  FormControl();
  pacientesFiltrados: Observable<Paciente[]>;

  constructor(
    private pacienteService: PacienteService,
    private signoService: SignoService,
    private route: ActivatedRoute,
    private router: Router,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fechaRegistro': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')

    });

    this.route.params.subscribe((data: Params) => {
      this.idSigno = data['id'];
      this.edicion = data['id'] != null;
      this.initForm();
    });

    this.listarPacientes();

    this.pacientesFiltrados = this.myControlPaciente.valueChanges.pipe(map(val => this.filtrarPacientes(val)));

  }
  get f() { return this.form.controls; }

  initForm() {
    //EDITAR, carga la data 
    if (this.edicion) {
      this.signoService.listarPorId(this.idSigno).subscribe(data => {
        console.log(data);
        this.form = new FormGroup({
          // console.log(data);

          'id': new FormControl(data.idSigno),
          'paciente': new FormControl(data.paciente),
          // 'paciente': this.myControlPaciente,
          'fechaRegistro': new FormControl(new Date()),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio),
          
        });
      });
    }
  }



  
  
  filtrarPacientes(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(el =>
        el.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || el.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || el.dni.includes(val.dni)
      );
    }    
    return this.pacientes.filter(el =>
      el.nombres.toLowerCase().includes(val?.toLowerCase()) || el.apellidos.toLowerCase().includes(val?.toLowerCase()) || el.dni.includes(val)
    );
  }

  aceptar(){

  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
    this.pacientes = data;
    });
  }

  mostrarPaciente(val: Paciente) {
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }


  registrar() {

    // if (this.form.invalid) { return; }
    
    let signo = new SignoVital;


    signo.paciente = this.form.value['paciente'];
    signo.idSigno = this.form.value['id'];
    signo.fechaRegistro = this.form.value['fechaRegistro'];
    signo.temperatura = this.form.value['temperatura'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

    if (this.edicion) {
      //MODIFICAR
      console.log("se va modificar");
      console.log(signo);
      this.signoService.modificar(signo).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('Se actualizo correctamente');
        });
      });
    } else {
      //INSERTAR
    console.log(signo);

    this.signoService.registrar(signo).subscribe(() => {
      this.snackBar.open("Se registró Correctamente", "Aviso", { duration: 2000 });

      setTimeout(() => {
        this.limpiarControles();
      }, 2000)

    });
    
    }
    this.router.navigate(['signo-vitales']);
  }

  limpiarControles() {
    this.fechaRegistro = new Date();
    this.fechaRegistro.setHours(0);
    this.fechaRegistro.setMinutes(0);
    this.fechaRegistro.setSeconds(0);
    this.fechaRegistro.setMilliseconds(0);
    this.mensaje = '';
    this.temperatura='';
    this.pulso='';
    this.ritmoRespiratorio='';
    
    this.myControlPaciente.reset();
  }


}
